"use strict";
var __extends = (this && this.__extends) || (function () {
  var extendStatics = function (d, b) {
    extendStatics = Object.setPrototypeOf ||
    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
    function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
  };
  return function (d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactMoneyComponent = /** @class */ (function (_super) {
  __extends(ReactMoneyComponent, _super);
  function ReactMoneyComponent(props) {
    var _this = _super.call(this, props) || this;
    _this.state = {
      val: props.value ? parseFloat(props.value).toFixed(props.config.precision) : parseFloat('0').toFixed(props.config.precision),
      configuration: props.config ? props.config : {
        prefix: '',
        suffix: '',
        precision: 2,
        className: '',
        decimalSeparator: ','
      },
      onChange: props.onChange ? props.onChange : undefined
    };
    return _this;
  }
  ReactMoneyComponent.prototype.componentDidMount = function () {
    this.valueChanged({ currentTarget: { value: this.state.val } });
  };
  ReactMoneyComponent.prototype.valueChanged = function (e) {
    var final = null;
    var decimals = null;
    var leftNumber = [];
    var number = e.currentTarget.value.replace(',', '');
    number = number.replace('.', '');
    if (isNaN(number)) {
      return false;
    }
    number = number.split('');
    var currentN = JSON.parse(JSON.stringify(number));
    if (currentN.length - this.state.configuration.precision <= 0) {
      decimals = number;
      leftNumber = ['0'];
    }
    else {
      decimals = number.splice(currentN.length - this.state.configuration.precision, number.length);
      leftNumber = number.splice(0, currentN.length - this.state.configuration.precision);
    }
    var separator = this.state.configuration.precision > 0 ? this.state.configuration.decimalSeparator : '';
    final = leftNumber.join('') + separator + decimals.join('');
    if (leftNumber.length >= 2) {
      final = final.replace(/^0/, '');
    }
    if (this.state.onChange)
      this.state.onChange(final);
    this.setState({ val: final });
  };
  ReactMoneyComponent.prototype.render = function () {
    var _this = this;
    return (React.createElement("div", { className: "reactMoneyInputGroup " + this.state.configuration.className },
    this.state.configuration.prefix !== '' ?
    React.createElement("div", { className: "reactMoneyInputGroupPrepend" },
    React.createElement("span", { className: "input-group-text" }, this.state.configuration.prefix)) : '',
    React.createElement("input", { type: 'text', className: "reactMoneyInput", value: this.state.val, onChange: function (e) { return _this.valueChanged(e); } }),
    this.state.configuration.suffix !== '' ?
    React.createElement("div", { className: "reactMoneyInputGroupAppend" },
    React.createElement("span", { className: "input-group-text" }, this.state.configuration.suffix)) : ''));
  };
  return ReactMoneyComponent;
}(React.Component));
exports.default = ReactMoneyComponent;
//# sourceMappingURL=indexDev.js.map
