import * as React from 'react';
export default class ReactMoneyComponent extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      val: props.value ? parseFloat(props.value).toFixed(props.config.precision) : parseFloat('0').toFixed(props.config.precision),
      configuration: props.config ? props.config : {
        prefix: '',
        suffix: '',
        precision: 2,
        className: '',
        decimalSeparator: ','
      },
      onChange: props.onChange ? props.onChange : undefined
    };
  }
  componentDidMount(){
    this.valueChanged({currentTarget:{value:this.state.val}});
  }
  valueChanged(e){
    var final = null;
    var decimals  = null;
    var leftNumber  = [];
    var number = e.currentTarget.value.replace(',', '');
    number = number.replace('.', '');
    if(isNaN(number)){
      return false;
    }
    number = number.split('');
    let currentN = JSON.parse(JSON.stringify(number));
    if(currentN.length - this.state.configuration.precision <= 0){
      decimals = number;
      leftNumber = ['0']
    } else {
       decimals = number.splice(currentN.length - this.state.configuration.precision, number.length);
       leftNumber = number.splice(0, currentN.length - this.state.configuration.precision);
    }
    let separator = this.state.configuration.precision > 0 ? this.state.configuration.decimalSeparator : '';
    final = leftNumber.join('') + separator + decimals.join('');
    if (leftNumber.length >= 2) {
      final = final.replace(/^0/, '');
    }
    if (this.state.onChange)
      this.state.onChange(final);
    this.setState({ val: final});
  }
  render(){
    return(
      <div className={"reactMoneyInputGroup " + this.state.configuration.className}>
        {
          this.state.configuration.prefix !== '' ?
          <div className="reactMoneyInputGroupPrepend">
            <span className="input-group-text">{this.state.configuration.prefix}</span>
          </div> : ''
        }
          <input type={'text'} className={"reactMoneyInput"} value={this.state.val} onChange={(e) => this.valueChanged(e)}/>
        {
          this.state.configuration.suffix !== '' ?
          <div className="reactMoneyInputGroupAppend">
            <span className="input-group-text">{this.state.configuration.suffix}</span>
          </div> : ''
        }
      </div>
    )
  }
}
