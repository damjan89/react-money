import * as React from 'react';
import * as ReactDOM from 'react-dom';
//@ts-ignore
import ReactMoneyComponent from 'react-currency-input-mask'
import 'react-currency-input-mask/src/assets/style.css'
export interface Istate {
    price?:any;
    config?:any;
}
export default class ReactMoneyExampleComponent extends React.Component<{}, Istate> {
  constructor(props:any) {
    super(props);
    this.state = {
      price: 2.2,
      config:  {
        prefix: '$',
        suffix: '%',
        precision: 2,
        className: '',
        decimalSeparator: ',',
      }
    }
  }
  changePrice(price:number){
   // this.setState({price: price})
  }
  render() {
    return (
    <div style={{width: '50%'}}>
      <ReactMoneyComponent value={this.state.price} config={this.state.config} onChange={(e:any) => this.changePrice(e)}></ReactMoneyComponent>
    </div>
    );
  }
}
ReactDOM.render(<ReactMoneyExampleComponent/>, document.getElementById('root'));
